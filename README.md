# FluentUdpStream

Client of [fluent-plugin-udp-stream](https://bitbucket.org/winebarrel/fluent-plugin-udp-stream).

## Download

[https://bitbucket.org/winebarrel/fluent_udp_stream/downloads/fluent_udp_stream-0.0.5.gem](https://bitbucket.org/winebarrel/fluent_udp_stream/downloads/fluent_udp_stream-0.0.5.gem)

## Installation

    $ gem install fluent_udp_stream-0.0.5.gem

## Usage

```ruby
# Please start Fluentd in advance
# And, please do the following:
#   while true; do
#     echo '{"hoge":"fuga"}' | fluent-cat debug.forward
#   done

require 'fluent_udp_stream'
require 'pp'

# Get records of every 10 seconds
FluentUdpStream.new.unit(10).each do |records|
  pp records
  # => [["debug.data", 1391707950, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707950, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707950, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707950, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707951, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707951, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707951, {"hoge"=>"fuga"}],
  #     ["debug.data", 1391707951, {"hoge"=>"fuga"}],
  #     ...
end
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
