require 'fluent_udp_stream/version'
require 'socket'
require 'msgpack'

if RUBY_VERSION =~ /\A1\.9\./
  begin
    require 'enumerable/lazy'
  rescue LoadError => e
    warn('`enumerable-lazy` is required in Ruby 1.9.x')
    raise e
  end
end

class FluentUdpStream
  include Enumerable

  BUFSIZE = 2 ** 16

  def initialize(options = {})
    options = {
      :host => '127.0.0.1',
      :port => 25000,
    }.merge(options)

    @sock = UDPSocket.open
    @sock.bind(options[:host], options[:port])
  end

  def each
    while msg = @sock.recv(BUFSIZE)
      begin
        msg = MessagePack.unpack(msg)
        yield(msg)
      rescue => e
        warn(e)
      end
    end
  end

  def unit(sec)
    prev_time = nil

    self.chunk {|tag, time, record|
      time
    }.slice_before {|time, records|
      result = (time % sec).zero? || (prev_time && minus_r(time, sec) != minus_r(prev_time, sec))
      prev_time = time
      result
    }.lazy.map {|records|
      records.inject([]) {|r, i| r + i[1] }
    }
  end

  private

  def minus_r(m, n)
    m - (m % n)
  end
end
