# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fluent_udp_stream/version'

Gem::Specification.new do |spec|
  spec.name          = "fluent_udp_stream"
  spec.version       = FluentUdpStream::VERSION
  spec.authors       = ["Genki Sugawara"]
  spec.email         = ["sgwr_dts@yahoo.co.jp"]
  spec.description   = %q{Client of fluent-plugin-stream-filter}
  spec.summary       = %q{Client of fluent-plugin-stream-filter}
  spec.homepage      = "https://bitbucket.org/winebarrel/fluent_udp_stream"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "msgpack"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
